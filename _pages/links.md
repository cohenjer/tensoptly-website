---
layout: page
title: Links
permalink: /links/
---

### Job offer links

- Recruitment website: [link](https://jobs.inria.fr/public/classic/en/offres/2020-02715) 
- Project proposal, in French: [link]({{site.baseurl}}/images/proposal.pdf)

### The work environment

- Panama team hosting the project: [link](https://team.inria.fr/panama/)
- Inria website: [link](https://www.inria.fr/en)
- IRISA website: [link](https://www.irisa.fr/en)
- Jeremy Cohen's website: [link](https://jeremy-e-cohen.jimdofree.com/)

### Programming environment

- Tensorly webpage: [link](http://tensorly.org)
- Tensorly github project: [link](https://github.com/tensorly/tensorly)
- Tensorly notebooks: [link](https://github.com/JeanKossaifi/tensorly-notebooks)

### Others

- This website's source code: [link](https://gitlab.com/cohenjer/tensoptly-website)
