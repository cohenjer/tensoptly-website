---
layout: page
title: Welcome to the Tensoptly webpage
---

### What is Tensoptly
Tensoptly is a development project, funded by Inria, which goal is to upgrade the [Tensorly](www.tensorly.org) python library for tensor decompositions with several enhanced features:
- state-of-the-art optimization algorithms
- class-based programming
- easy customization

### Explore
If you are interested in the project, see [The project]({{ site.baseurl }}/project) page.

If you are interested in the engineer position offered in the project, see [Job offer]({{ site.baseurl }}/job).

Useful links to people and organisations involved in Tensoptly is provided in [Links]({{ site.baseurl }}/links).

To ask any question relative to the project, write-me an email (link at the bottom).

##### Credits:
Icons made by <a href="http://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>,
template forked from [the Reverie template](https://jekyllthemes.io/theme/reverie)
